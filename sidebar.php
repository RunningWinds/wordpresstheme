<?php
if (!is_active_sidebar('right-sidebar')) return;
?>
<div class="col-3">
    <div class="card">
        <div class="card-header">
            <?php wp_strip_all_tags(dynamic_sidebar('right-sidebar'),0); ?>
        </div>
    </div>
</div>